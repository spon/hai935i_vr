import numpy as np
import cv2 as cv

# corrPoints = [('Xg', 'Yg', 1), ('Xd', 'Yd', 1)]
corrPoints = []
PointCoeffs = []
F = []
height = 0
width = 0
clickedRight = False


# MISC FUNCTIONS
def makePairs(tab):
    return [tab[n:n+2] for n in range(0, len(tab), 2)]

def PairsToMatrice(pairs, mode2 = False):
    A = []
    for pair in pairs:
        row = []
        for i in range(3):
            for j in range(3):
                row.append(pair[0][i] * pair[1][j])  

        # UNCOMMENT TO DEBUG        
        # print("ROW: ", end='')
        # print(np.array([row]))
        if not mode2:
            A.append(row.copy()[:-1])
        else:
            A.append(row.copy())
        row.clear()

    return np.array(A)

def computeFMatrix():
    print("Computing matrix ...")
    pairs = makePairs(corrPoints)
    A = np.asmatrix(PairsToMatrice(pairs))
    B = np.zeros(len(A))
    B.fill(-1)

    # print(A)
    # print("B Vector: ", end='')
    # print(B)

    #Compute approximation of f: (At * A)^(-1) * At * B
    # leftComp = np.linalg.inv(np.matmul(At, A))
    f = np.matmul(np.linalg.pinv(A), B)

    F = np.squeeze(np.asarray(f))
    F = np.append(F, [1])
    print(F)
    print('F computed with method 1 :')
    F = np.reshape(F, (3, 3))
    print(F)  # F: Fondamental matrix Ld ) Ft * Mg (F transpose * (Xg, Yg, 1)) = Ld(a,b,c) (coefficients of line in image right)
    corrPoints.clear()
    return F

def computeFMatrix2():
    print("Computing matrix ...")
    pairs = makePairs(corrPoints)
    C = np.asmatrix(PairsToMatrice(pairs, mode2=True))

    D = np.transpose(C) @ C
    u, s, vh = np.linalg.svd(D)
    # eigval, eigvec = np.linalg.eig(D)
    # print("eigval = ", eigval)
    # print("eigvec = ", eigvec)

    # f = eigvec[-1]
    eigvec = vh[8]
    f = eigvec
    print("eigenvector of min value: ", f)
    # print(A)
    # print("B Vector: ", end='')
    # print(B)

    # #Compute approximation of f: (At * A)^(-1) * At * B
    # # leftComp = np.linalg.inv(np.matmul(At, A))
    # f = np.matmul(np.linalg.pinv(A), B)

    # F = np.squeeze(np.asarray(f))
    # F = np.append(F, [1])
    # print(F)
    # print('F computed with method 1 :')
    F = np.reshape(np.asarray(f), (3, 3))
    print(F)  # F: Fondamental matrix Ld ) Ft * Mg (F transpose * (Xg, Yg, 1)) = Ld(a,b,c) (coefficients of line in image right)
    corrPoints.clear()
    return F

# EVENT HANDLERS
def clickEventLeft(event, x, y, flags, params):
    global clickedRight
    # checking for left mouse clicks
    if event == cv.EVENT_LBUTTONDOWN:       
        cv.line(imgL, (x-5, y), (x+5, y), (0, 0, 255), 1)
        cv.line(imgL, (x, y-5), (x, y+5), (0, 0, 255), 1)

        params[0].append((x,y,1))
        cv.imshow('imageL', imgL)
        # params[1] = False
        clickedRight = False

def clickEventRight(event, x, y, flags, params):
    global clickedRight
    # checking for left mouse clicks
    if event == cv.EVENT_LBUTTONDOWN:       
        cv.line(imgR, (x-5, y), (x+5, y), (0, 0, 255), 1)
        cv.line(imgR, (x, y-5), (x, y+5), (0, 0, 255), 1)

        params[0].append((x,y,1))
        cv.imshow('imageR', imgR)
        print(clickedRight)
        # params[1] = True
        clickedRight = True


def traceLine(img, a, b, c, traceRight):
    m = img.shape[1]
    n = img.shape[0]
    
    #ax + by + c = 0
    if a == 0:
        p1 = (0, int(-c / b))
        p2 = (n, int(-c / b))

    elif b == 0:
        p1 = (int(-c / a), 0)
        p2 = (int(-c / a), m)
    
    elif c == 0:
        p1 = (0, 0)
        p2 = (n, int((-a * n) / b))
    
    else:
        p1 = (int(-c / a), 0)
        p2 = max((0, int(-c / b)), (int((-b * m - c) / a), n)) #pick intersection farther intersection


    print("Drawing", a, "x ", b, "y ", c, " = 0, MAX line ", m, " Max col: ", n)
    print("first point at ", p1, " second at ", p2)
    print(img)
    cv.line(img, p1, p2, (0, 255, 0), 2)

    if traceRight:
        cv.imshow('imageR', img)    
    else:
        cv.imshow('imageL', img)


def trace_line_with_param(a, b, c, image, traceRight):
    def equation(x):
        try:
            return (- c - a * x) / b
        except ZeroDivisionError:
            return None

    def inverse_equation(y):
        try:
            return (- c - b * y) / a
        except ZeroDivisionError:
            return None

    shape = image.shape
    eq_0 = equation(0)
    eq_shape = equation(shape[1])

    if eq_0 is None and eq_shape is None:
        window.statusBar().showMessage("Can't draw a line with the 2 same points", 1000)
        return
    elif eq_0 is None or eq_shape is None:
        pos_1 = [round(inverse_equation(0)), shape[0]]
        pos_2 = [round(inverse_equation(shape[0])), 0]
    else:
        pos_1 = (0, round(eq_0))
        pos_2 = (shape[1], round(eq_shape))

    cv.line(image, pos_2, pos_1,  (0, 255, 0), 1)

    if traceRight:
        cv.imshow('imageR', image)    
    else:
        cv.imshow('imageL', image)

if __name__ == '__main__':
    imgL = cv.imread("img/TurtleG.tif")
    imgR = cv.imread("img/TurtleD.tif")
    # imgL = cv.imread("img/ArbreG.tif")
    # imgR = cv.imread("img/ArbreD.tif")
    # cv.namedWindow("image")
    # img = np.concatenate((imgL, imgR), axis=1)

    pressed = False
    
    # switch = '0 : Select pairs \n1 : Compute F matrix\n2 : Click point of an image\n'
    # cv.createTrackbar(switch, "image", 0, 2, sliderEvent)
    height = imgL.shape[0]
    width = imgL.shape[1]
    # nH = img.shape[0]
    # nW = img.shape[1]

    cv.imshow("imageL", imgL)
    cv.imshow("imageR", imgR)
    cv.setMouseCallback("imageL", clickEventLeft, [corrPoints, clickedRight])
    cv.setMouseCallback("imageR", clickEventRight, [corrPoints, clickedRight])

    # traceLine(img, 5, -20, 150)
    # k = cv.waitKey(0)
    while (True):
        # cv.imshow("image", imgL)
        # cv.imshow("image", imgR)
        # print("zzz")
        if cv.waitKey(20) == 27:
            break

        elif cv.waitKey(20) == ord('y'):
            pressed = False
            print("resetting pressed")

        elif cv.waitKey(20) == ord('m') and not pressed:
            print("z")
            pressed = True
            F = computeFMatrix()
            # F = computeFMatrix2()

        elif cv.waitKey(20) == ord('l') and not pressed:
            pressed = True
            print("Point to test? ", end="")
            print(corrPoints)
            print("img right clicked ?", end="")
            print(clickedRight)
            Coeffs = np.array((corrPoints[0][0], corrPoints[0][1], 1))
            # clickedRight = False
            # L = []
            # if(corrPoints[0][0] > height or corrPoints[0][1] > width):
            #     clickedRight = True
            
            if clickedRight:
                print("clicked right image, line will appear in left image")
                L = F @ Coeffs
                trace_line_with_param( L[0], L[1], L[2], imgL, False)
            else:
                print("clicked left image, line will appear in right image")
                Ft = np.transpose(F)
                # print("SHAPES {} * {}", F.shape, Coeffs.shape)
                # L = Ft @ np.transpose(Coeffs)
                print(F)
                print(Coeffs)

                L = Ft @ Coeffs
                trace_line_with_param(L[0], L[1], L[2], imgR, True)
            print("TRACING LINE")
            print(L)
            corrPoints.clear()
    cv.destroyAllWindows()
        